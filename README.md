# ORCONF2023

Kindly find my presentation from ORCONF2023 here, including links to background materials and references.

## References
- [PowerSensor3]( https://git.astron.nl/RD/powersensor3-demo)
- [ASTRON Firmware](https://git.astron.nl/rtsd/hdl)
- [ASTRON RDMA Firmware (under development)](https://git.astron.nl/rtsd/hdl/-/tree/master/applications/rdma_demo/libraries?ref_type=heads)
- [LOFAR: FOSS HPC across 2000 kilometers; Corne Lukken; FOSDEM2023](https://archive.fosdem.org/2023/schedule/event/lofar_foss_hpc/)

## Background materials
- [Commodity compute and data-transport system design in modern large-scale distributed radio telescopes](https://www.astron.nl/~broekema/Thesis/PhD-Thesis.pdf). Chapters: 1 Introduction and 5 COBALT: GPU Correlator
- [LOFAR Overview article](https://www.aanda.org/articles/aa/full_html/2013/08/aa20873-12/aa20873-12.html)
- [LOFAR overview paper](https://ui.adsabs.harvard.edu/abs/2013A%26A...556A...2V/abstract)
- [Interactive LOFAR map](https://www.astron.nl/lofartools/lofarmap.html)
- [LOFAR Google streetview](https://www.google.nl/maps/place/LOFAR+superterp/@52.9152576,6.8698279,3a,75y,355.19h,92.79t/data=!3m8!1e1!3m6!1sAF1QipN2u8ZR3aIhDLJFrIu_FtyKgENWZPMSMLcxkXj5!2e10!3e11!6shttps:%2F%2Flh5.googleusercontent.com%2Fp%2FAF1QipN2u8ZR3aIhDLJFrIu_FtyKgENWZPMSMLcxkXj5%3Dw203-h100-k-no-pi0-ya340-ro-0-fo100!7i5376!8i2688!4m5!3m4!1s0x47b7dc6b05b8f321:0xa95c77b8907fe991!8m2!3d52.9152576!4d6.8698279)
- [RDMA feasibility study](https://repository.tudelft.nl/islandora/object/uuid%3A94d16d94-e4c7-4a54-a26a-af114db02dd7)
- [RDMA feasiblity study short read (pdf download; Maxwell 26.1 P10-13)](https://etv.tudelft.nl/file/1033?view?view=true)

## Contact
Steven van der Vlugt: vlugt@astron.nl

## License
Licensed under Apache 2.0, see license file

